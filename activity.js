
db.rooms.insertOne({
	"name": "single",
	"accommodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities.",
	"roomsAvailable": 10,
	"isAvailable": false
});

db.rooms.insertMany([
		{
			"name": "double",
			"accommodates": 3,
			"price": 2000,
			"description": "A room fit for small family group on a vacation.",
			"roomsAvailable": 5,
			"isAvailable": false
		},
		{
			"accommodates": 4,
			"price": 40000,
			"description": "A room with a queen sized bed perfect for a simple getaway.",
			"roomsAvailable": 15,
			"isAvailable": false
		}
	]);

db.rooms.find(
	{ 
		"name": "double"
	}
);

db.rooms.updateOne(
	{"description": "A room with a queen sized bed perfect for a simple getaway."},
	{
		$set:{
			"name": "queen",
			"roomsAvailable": 0
		}
	}
);

db.rooms.deleteMany({
	"roomsAvailable": 0
});
